
Loading Animation
=======

简单的页面loading动画效果

## 使用方法

```javascript
$('xxx').loading();

// $() 默认等于 $('body')
$().loading('show'); // 显示动画
$().loading('hide'); // 隐藏动画

$('#id').loading({fixed: false}); // 指定元素
$('#id').loading({show: false, fixed: false}); // 默认隐藏
```



## 参数

```json
{
  fixed: true, // css fixed 如果不需要全屏显示，则设置为false
  show: true // 默认是否显示
}
```