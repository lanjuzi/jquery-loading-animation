/**
 * jquery loading动画效果
 *
 * @author lanjuzi
 * @version $Id$
 */
(function ($) {
    var defaults = {
        fixed: true,
        show: true
    };
    function Loading(el, options) {
        this.options = $.extend({}, defaults, options);
        this.el = el;
        this.init();
    }

    Loading.prototype = {
        init: function () {
            this.$container = $('<div class="loading-animation-container loading-animation-mask loading-animation-fixed"><div class="loading-animation"><div class="loading-animation-c1"></div><div class="loading-animation-c2"></div></div></div>');

            if (!this.options.fixed) {
                this.$container.removeClass('loading-animation-fixed');
                if(this.el.style.position === '') {
                    this.el.style.position = 'relative';
                }
            }
            this.$container.toggle(this.options.show);
            this.$container.appendTo($(this.el));
            if (this.options.fixed && this.options.show) {
                $('body').addClass('loading-animation-open');
            }
        },
        show: function() {
            if (this.options.fixed) {
                $('body').addClass('loading-animation-open');
            }
            this.$container.show();
        },
        hide: function() {
            if (this.options.fixed) {
                $('body').removeClass('loading-animation-open');
            }
            this.$container.hide();
        },
        toggle: function () {
            this.$container.toggle();
            if (this.options.fixed) {
                if (this.$container.is(':hidden')) {
                    $('body').removeClass('loading-animation-open');
                } else {
                    $('body').addClass('loading-animation-open');
                }
            }
        }
    };

    $.fn['loading'] = function(options) {
        var target = this;
        if (this.length === 0) {
            target = $('body');
        }
        target.each(function() {
            if (!$.data(this, '__loading')) {
                var o = new Loading(this, options);
                $.data(this, '__loading', o);
            } else {
                var self = $.data(this, '__loading'), method = 'show';

                if (typeof options === 'string') {
                    var allows = {
                        show: true,
                        hide: true,
                        toggle: true
                    };
                    if (!allows[options]) {
                        return false;
                    }
                    method = options;
                    options = null;
                }
                self[method].call(self, options);

                return false;
            }
        });
    };
})(jQuery);